#!/bin/sh

coverage run --source='.' manage.py test --verbosity=2 "$@"

if [ $? -eq 0 ] ; then
  coverage report
else
  exit 1
fi
