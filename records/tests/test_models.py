import numpy as np
from django.test import TestCase
from records.models import Record
from records.tests import create_test_record


class RecordModelTest(TestCase):

    def test_saving_and_retrieving_record_works(self):
        test_record = create_test_record()

        Record.objects.create(**test_record)
        saved_records = Record.objects.all()

        self.assertEqual(saved_records.count(), 1)
        for field, content in test_record.items():
            self.assertEqual(getattr(saved_records[0], field), content)

    def test_xydata_property_returns_the_parsed_csv_data(self):
        record = Record(**create_test_record(data="-2,4\n-1,1\n0,0\n1,1\n2,4"))
        x, y = record.xydata
        np.testing.assert_array_equal(x, np.array([-2, -1, 0, 1, 2]))
        np.testing.assert_array_equal(y, np.array([4, 1, 0, 1, 4]))

    def test_can_evaluate_if_data_describes_closed_polygon(self):
        with self.subTest("open polygon"):
            record = Record(**create_test_record(
                data="-2,4\n-1,1\n0,0\n1,1\n2,4"))
            self.assertFalse(record.is_closed_polygon)

        with self.subTest("closed polygon"):
            dataset = Record(**create_test_record(
                 data="-2,4\n-1,1\n0,0\n1,1\n-2,4"))
            self.assertTrue(dataset.is_closed_polygon)

    def test_str_representation_returns_title(self):
        record = Record(**create_test_record(title="foobar"))
        self.assertEqual(str(record), record.title)


class RecordManagerTest(TestCase):

    def setUp(self):
        self.record1 = Record.objects.create(
            **create_test_record(title="Awesome Axion paper"))
        self.record2 = Record.objects.create(
            **create_test_record(title="Another Axion work"))

    def test_query_on_title(self):
        with self.subTest("case insensitive"):
            qs = Record.objects.search("aXiOn")
            self.assertIn(self.record1, qs)
            self.assertIn(self.record2, qs)

        with self.subTest("including spaces"):
            qs = Record.objects.search("Axion work")
            self.assertNotIn(self.record1, qs)
            self.assertIn(self.record2, qs)

            qs = Record.objects.search("    Axion    work  ")
            self.assertNotIn(self.record1, qs)
            self.assertIn(self.record2, qs)

    def test_query_on_tags(self):
        self.record1.tags.add("lsw")
        self.record1.tags.add("scalar")
        self.record2.tags.add("LSW")
        self.record2.tags.add("pseudoscalar")

        with self.subTest("single tag"):
            qs = Record.objects.search("tag:lsw")
            self.assertIn(self.record1, qs)
            self.assertIn(self.record2, qs)

        with self.subTest("two tags"):
            qs = Record.objects.search("tag:scalar   tag:lsw")
            self.assertIn(self.record1, qs)
            self.assertNotIn(self.record2, qs)

    def test_query_on_tags_and_tile(self):
        self.record1.tags.add("lsw")
        self.record1.tags.add("scalar")
        self.record2.tags.add("LSW")
        self.record2.tags.add("pseudoscalar")

        qs = Record.objects.search("Axion  tag:scalar")
        self.assertIn(self.record1, qs)
        self.assertNotIn(self.record2, qs)
