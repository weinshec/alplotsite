from django.test import TestCase
from records.forms import RecordForm
from records.tests import create_test_record


class RecordFormTest(TestCase):

    def test_form_shows_errors_for_missing_but_not_optional_fields(self):
        form = RecordForm(data={})

        required_fields = ["title", "author", "data"]
        optional_fields = ["abstract", "journal", "doi", "arxiv"]

        self.assertFalse(form.is_valid())
        for field in required_fields:
            self.assertIn(field, form.errors)
        for field in optional_fields:
            self.assertNotIn(field, form.errors)

    def test_csv_field_validates_correct_data(self):
        with self.subTest("valid"):
            form = RecordForm(data=create_test_record())
            self.assertTrue(form.is_valid())

        with self.subTest("inconsitent number of columns"):
            form = RecordForm(data=create_test_record(data="0,1,2\n1,1\n2,4"))
            self.assertFalse(form.is_valid())

        with self.subTest("non-numeric data"):
            form = RecordForm(data=create_test_record(data="0,is\n1,inv\n"))
            self.assertFalse(form.is_valid())

        with self.subTest("too few rows"):
            form = RecordForm(data=create_test_record(data="0,1\n"))
            self.assertFalse(form.is_valid())

            form = RecordForm(data=create_test_record(data="\n"))
            self.assertFalse(form.is_valid())

        with self.subTest("not exactly 2 columns"):
            form = RecordForm(data=create_test_record(data="0\n1\n2"))
            self.assertFalse(form.is_valid())

            form = RecordForm(data=create_test_record(data="0,1,2\n3,4,5"))
            self.assertFalse(form.is_valid())
