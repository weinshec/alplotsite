from django.contrib.admin.sites import AdminSite
from records.admin import RecordAdmin
from records.models import Record
from records.tests import create_test_record
from users.tests import UsersTestCase


class MockRequest:
    pass


request = MockRequest()


class RecordAdminTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.record = Record.objects.create(
            **create_test_record(creator=self.alice))
        self.site = AdminSite()

    def test_has_image_field(self):
        admin = RecordAdmin(Record, self.site)
        self.assertIn(
            "image", admin.get_readonly_fields(request, self.record))

    def test_image_method_returns_img_tag(self):
        admin = RecordAdmin(Record, self.site)
        image = admin.image(self.record)
        self.assertIn("<img", image)
        self.assertIn(self.record.render.url, image)
