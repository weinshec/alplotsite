import matplotlib.pyplot as plt
import numpy as np
import re
import warnings
from django.contrib.auth.models import User
from django.db import models
from django.dispatch import receiver
from django.shortcuts import reverse
from guardian.shortcuts import assign_perm
from io import StringIO
from records.validators import validate_csv
from taggit.managers import TaggableManager
from webplots.models import RenderMixin


class RecordSearchQuerySet(models.QuerySet):

    TAGS_RE = re.compile('\s*tag:(?P<tag>\w+)\s*')

    def search(self, query):
        title, tags = self._parse_query(query)
        qs = self
        if title:
            qs = qs.filter(title__icontains=title)
        for tag in tags:
            qs = qs.filter(tags__name__iexact=tag).distinct()
        return qs

    def _parse_query(self, query):
        # Find and remove tags from query
        tags = re.findall(self.TAGS_RE, query)
        title = re.sub(self.TAGS_RE, "", query)
        # Remove multiple whitespaces from title
        title = re.sub("\s+", " ", title).strip()
        return title, tags


class Record(RenderMixin, models.Model):
    title = models.CharField(max_length=256)
    abstract = models.TextField(blank=True)
    author = models.TextField()
    journal = models.CharField(max_length=128, blank=True)
    doi = models.CharField(max_length=64, blank=True)
    arxiv = models.CharField(max_length=32, blank=True)
    data = models.TextField(validators=[validate_csv])
    creation_date = models.DateField(auto_now_add=True)
    last_update = models.DateField(auto_now=True)
    creator = models.ForeignKey(User,
                                null=True,
                                editable=False,
                                on_delete=models.CASCADE)
    tags = TaggableManager(blank=True)

    objects = RecordSearchQuerySet.as_manager()

    class Meta:
        permissions = (
            ("view_record", "Can view record"),
        )

    def __str__(self):
        return self.title

    @property
    def xydata(self):
        with warnings.catch_warnings(record=False):
            warnings.simplefilter("ignore")
            data = np.loadtxt(StringIO(self.data), delimiter=",", unpack=True)
        return data

    @property
    def is_closed_polygon(self):
        x, y = self.xydata
        return x[0] == x[-1] and y[0] == y[-1]

    def get_absolute_url(self):
        return reverse("record_detail", args=[self.pk])

    def draw(self):
        fig, ax = plt.subplots()

        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_xlabel("mass / eV")
        ax.set_ylabel(r"$g_{a\gamma\gamma} / GeV^{-1}$")

        ax.set_xlim(1e-5, 1e-2)
        self.draw_on(axes=ax, adj_axis="both", fill=True)

        return fig

    def draw_on(self, axes, adj_axis=None, fill=False, **kwargs):
        x, y = self.xydata

        if adj_axis in ["x", "both"]:
            axes.set_xlim(np.min(x), np.max(x))
        if adj_axis in ["y", "both"]:
            if self.is_closed_polygon:
                axes.set_ylim(np.min(y), np.max(y))
            else:
                axes.set_ylim(np.min(y)/10, np.max(y)*10)

        label = kwargs.pop("label", None)

        if self.is_closed_polygon and fill:
            return axes.fill(x, y, label=label, **kwargs)

        if fill:
            return axes.fill_between(x, y, axes.get_ylim()[1],
                                     label=label,
                                     facecolor=kwargs.get("color"),
                                     alpha=kwargs.get("alpha", 1.0),
                                     interpolate=True,
                                     zorder=kwargs.get("zorder", 0)+0.5)

        plot = axes.plot(x, y, label=label, **kwargs)
        if not self.is_closed_polygon:
            axes.vlines([x[0], x[-1]],
                        ymin=[y[0], y[-1]],
                        ymax=axes.get_ylim()[1],
                        **kwargs)
        return plot


@receiver(models.signals.post_save, sender=Record)
def record_post_save(sender, **kwargs):
    record, created = kwargs["instance"], kwargs["created"]
    if created and record.creator:
        assign_perm("records.view_record", record.creator, record)
        assign_perm("records.change_record", record.creator, record)
        assign_perm("records.delete_record", record.creator, record)
