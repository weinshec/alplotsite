from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, DetailView, ListView,\
                                 UpdateView
from guardian.decorators import permission_required_or_403
from guardian.mixins import PermissionListMixin, PermissionRequiredMixin
from io import StringIO
from listsearch.views import ListSearchMixin
from records.models import Record
from records.forms import RecordForm
from webplots.views import PreviewMixin


class RecordNew(PermissionRequiredMixin, PreviewMixin, CreateView):
    model = Record
    form_class = RecordForm
    template_name = "records/new.html"

    permission_required = "records.add_record"
    permission_object = None

    preview_object = None

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.creator = self.request.user
        self.object.save()
        form.save_m2m()
        return redirect(self.object.get_absolute_url())

    def render_preview(self, context):
        form = context["form"]
        form_data = form.initial

        if form.is_bound:
            form.is_valid()
            form_data = form.cleaned_data

        return Record(**form_data).draw()


class RecordDetail(PermissionRequiredMixin, DetailView):
    model = Record
    template_name = "records/detail.html"

    permission_required = "records.view_record"
    return_403 = True


class RecordUpdate(PermissionRequiredMixin, PreviewMixin, UpdateView):
    model = Record
    form_class = RecordForm
    template_name = "records/new.html"

    permission_required = "records.change_record"
    return_403 = True

    def render_preview(self, context):
        form = context["form"]
        form_data = form.initial

        if form.is_bound:
            form.is_valid()
            form_data = form.cleaned_data

        return Record(**form_data).draw()


class RecordDelete(PermissionRequiredMixin, DeleteView):
    model = Record
    success_url = reverse_lazy("record_list")
    template_name = "records/delete.html"

    permission_required = "records.delete_record"
    return_403 = True


class RecordList(PermissionListMixin, ListSearchMixin, ListView):
    model = Record
    context_object_name = "records"
    template_name = "records/list.html"

    permission_required = "records.view_record"

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)
        if self.query:
            return queryset.search(self.query)
        return queryset


@permission_required_or_403("records.view_record", (Record, 'pk', 'pk'))
def download_as_csv(request, pk):
    record = Record.objects.get(pk=pk)

    csv_file = StringIO()
    csv_file.write(record.data)

    response = HttpResponse(csv_file.getvalue(), content_type="text/plain")
    response["Content-Disposition"] = \
        "attachment; filename=record_{}.csv".format(pk)

    return response
