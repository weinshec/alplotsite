#!/usr/bin/env python

from pathlib import Path
from setuptools import setup, find_packages

with Path("./README.md").open(encoding="utf-8") as f:
    long_description = f.read()

info = {
    "name":                "alplotsite",
    "version":             "0.1.5",
    "author":              "Christoph Weinsheimer",
    "author_email":        "weinshec@students.uni-mainz.de",
    "url":                 "https://gitlab.com/weinshec/alplotsite",
    "packages":            find_packages(),
    "description":         "Axion-Like-Particle exclusion limit plotting",
    "long_description":    long_description,
    "keywords":            ["axion", "ALPs", "LSW"],
    "install_requires":    [
                                "numpy",
                                "matplotlib>=2.0",
                                "pint",
                                "Pillow",
                                "django>=2.0",
                                "django-sniplates>=0.5.0",
                                "django-allauth>=0.35.0",
                                "django-guardian>=1.4.9",
                                "easy-thumbnails>=2.5",
                                "dealer",
                                "django-taggit>=0.22.2",
                                "django-webplots",
                                "django-listsearch",
                           ],
    "setup_requires":      ["nose", "flake8", "coverage"],
    "dependency_links":    [
                                "git+https://gitlab.com/weinshec/django-webplots.git#egg=django-webplots",
                                "git+https://gitlab.com/weinshec/django-listsearch.git#egg=django-listsearch",
                           ],
}

if __name__ == "__main__":
    setup(**info)
