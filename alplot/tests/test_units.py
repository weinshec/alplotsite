import unittest
from alplot import units, photon_energy, ParameterSet
from pint.errors import DimensionalityError


class UnitsTest(unittest.TestCase):

    def test_heaviside_lorentz_conversions(self):
        with self.subTest("mass"):
            m_e = 1. * units.m_e
            m_keV = m_e.to("keV", "hlu")
            self.assertAlmostEqual(m_keV.magnitude, 511, delta=0.5)
            self.assertEqual(m_keV.units, units.keV)

        with self.subTest("length"):
            length = 1. * units.centimeter
            l_hlu = length.to("eV^-1", "hlu")
            self.assertAlmostEqual(l_hlu.magnitude, 50677, delta=0.5)
            self.assertEqual(l_hlu.units, units.eV**-1)

        with self.subTest("magnetic field"):
            B = 1. * units.T
            B_hlu = B.to("eV^2", "hlu")
            self.assertAlmostEqual(B_hlu.magnitude, 195, delta=0.5)
            self.assertEqual(B_hlu.units, units.eV**2)

    def test_photon_conversion(self):
        with self.subTest("valid wavelength"):
            length = 1064 * units.nanometer
            E = photon_energy(wavelength=length)
            self.assertAlmostEqual(E.magnitude, 1.16, delta=0.01)
            self.assertEqual(E.units, units.eV)

        with self.subTest("dimensionless"):
            with self.assertRaises(DimensionalityError):
                photon_energy(wavelength=42)


class ParameterSetTest(unittest.TestCase):

    def setUp(self):
        ParameterSet.defaults = {
            "length": 42.0 * units.m,
        }

    def tearDown(self):
        ParameterSet.defaults = {}

    def test_init_overwrites_defaults(self):
        pset = ParameterSet(length=21*units.m)
        self.assertEqual(pset.length.magnitude, 21)
        self.assertEqual(pset.length.units, units.m)

    def test_keys_not_in_defaults_will_be_ignored(self):
        pset = ParameterSet(foo=1*units.s)
        self.assertFalse(hasattr(pset, "foo"))

    def test_setter_enforces_unit(self):
        with self.subTest("Incompatible unit"):
            with self.assertRaises(DimensionalityError):
                ParameterSet(length=42*units.s)

        with self.subTest("No unit"):
            with self.assertRaises(DimensionalityError):
                ParameterSet(length=42)

        with self.subTest("valid conversion"):
            try:
                ParameterSet(length=42*units.cm)
            except DimensionalityError:
                self.fail("DimensionalityError raised although not expected")
