import numpy as np
import unittest
from alplot import lsw, units


class LSWSetupTest(unittest.TestCase):

    def test_setup_has_default_attributes(self):
        setup = lsw.LSWSetup()
        self.assertIsInstance(setup.pset.wavelength, units.Quantity)
        self.assertIsInstance(setup.pc, lsw.Cavity)

    def test_setup_init_overwrites_defaults(self):
        setup = lsw.LSWSetup(wavelength=1064*units.nm)
        self.assertEqual(setup.pset.wavelength.magnitude, 1064)

    def test_setup_init_sets_pc_and_rc_correct(self):
        setup = lsw.LSWSetup(pc=lsw.Cavity(bfield=42*units.T))
        self.assertEquals(setup.pc.bfield, 42*units.T)
        self.assertEquals(setup.rc.bfield, lsw.Cavity.defaults["bfield"])

    def test_calculating_coupling_constant(self):
        setup = lsw.LSWSetup()

        with self.subTest("modified conversion probability"):
            prob_mod = setup.prob_mod(1*units.meV, "pc")
            expec_dimension = (1.*units.eV**2).dimensionality
            self.assertEquals(prob_mod.dimensionality, expec_dimension)

        with self.subTest("couping constant"):
            gagg = setup.gagg(1*units.meV)
            self.assertEqual(gagg.units, units.GeV**-1)

        with self.subTest("coupling constant numpy array"):
            m = np.linspace(1, 10, 100) * units.meV
            g = setup.gagg(m)
            self.assertEqual(len(g), 100)
