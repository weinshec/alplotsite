import numpy as np
import unittest
from alplot import units, vmb


class VMBSetupTest(unittest.TestCase):

    def test_setup_has_default_attributes(self):
        setup = vmb.VMBSetup()
        self.assertIsInstance(setup.pset.wavelength, units.Quantity)

    def test_setup_init_overwrites_defaults(self):
        setup = vmb.VMBSetup(wavelength=523*units.nm)
        self.assertEqual(setup.pset.wavelength.magnitude, 523)

    def test_calculating_coupling_constant(self):
        setup = vmb.VMBSetup()

        with self.subTest("couping constant"):
            gagg = setup.gagg(1*units.meV)
            self.assertEqual(gagg.units, units.GeV**-1)

        with self.subTest("coupling constant numpy array"):
            m = np.linspace(1, 10, 100) * units.meV
            g = setup.gagg(m)
            self.assertEqual(len(g), 100)
