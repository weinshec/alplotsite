#!/usr/bin/env python
# encoding: utf-8

import os
from alplotsite.settings.base import *


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Database
# TODO: Fill details or environment variable
DATABASES = {
}
