#!/usr/bin/env python
# encoding: utf-8

import os
from alplotsite.settings.base import *


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# Allowed hosts
ALLOWED_HOSTS = ['*']

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '../database/db.sqlite3'),
    }
}

# Fixtures
FIXTURE_DIRS = [
    os.path.join(BASE_DIR, "fixtures"),
]

# static files root
STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR, '../static'))
MEDIA_ROOT = os.path.abspath(os.path.join(BASE_DIR, '../media'))
