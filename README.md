# alplotsite

[![build status](https://gitlab.com/weinshec/alplotsite/badges/master/build.svg)](https://gitlab.com/weinshec/alplotsite/commits/master)
[![coverage report](https://gitlab.com/weinshec/alplotsite/badges/master/coverage.svg)](https://gitlab.com/weinshec/alplotsite/commits/master)

Web Application to create exclusion limit plots for Axion-like particles written
in [django](https://www.djangoproject.com/).

## Applications
The project utilizes several django-applications including third-party apps
performing a lot important work. The following list gives an overview:

### Specific applications
+ __records__

  Provides the models to represent the available physics data. Defines the main
  model class `Record` holding the fields common to all dataset
  types, e.g. `title`, `author`, and implements the `draw` method defining how
  to plot this data using _matplotlib_.

+ __limitplots__

  The `LimitPlot` model represents an accumulation of records rendering them
  in a combined plot. Besides choosing the records to be included in the plot
  the model also feature general fields like axis ranges, title and decription.
  The `Record` models are included through an intermediate `Visualization` Model
  in a Many-to-Many relationship, holding additional fields like `colo`r and
  `label` to render the corresponding `Record` with.

### External applications
+ [django-webplots](https://gitlab.com/weinshec/django-webplots):
  Single purpose _ModelMixin_ providing an easy updatable `render` ImageField.
  Models using this mixin (such as `Records` and `LimitPlot`) only need to
  overwrite the `draw()` method return a matplotlib figure which is stored in
  as `.png` image automatically by the mixin on every model `save()`.

+ [django-sniplates](http://sniplates.readthedocs.io/en/latest/):
  Efficient way to provide template snippets. Mostly used for form field
  rendering.

+ [easy-thumbnails](http://easy-thumbnails.readthedocs.io/en/latest/):
  Automatic creation of image thumbnails used in `Record`/`LimitPlot` list
  views.

+ [django-allauth](https://django-allauth.readthedocs.io/en/latest/):
  Application providing user registration and authentication functionality for
  Django-powered Web sites. Used for the user registration on __ALPlot__.

+ [django-guardian](http://django-guardian.readthedocs.io/en/latest/):
  Object-Level permissions for django. Used to have `Datasets` and `LimitPlot`
  only be visible/editable/deletable by the user that created them.
