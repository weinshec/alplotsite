import matplotlib.pyplot as plt
import re
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator, \
                                   RegexValidator
from django.db import models
from django.dispatch import receiver
from django.shortcuts import reverse
from guardian.shortcuts import assign_perm
from limitplots.validators import validate_finite_positive
from records.models import Record
from webplots.models import RenderMixin


class LimitPlot(RenderMixin, models.Model):

    LEGEND_LOC = (
        ("best", "best"),
        ("upper right", "upper right"),
        ("upper left", "upper left"),
        ("lower left", "lower left"),
        ("lower right", "lower right"),
        ("center left", "center left"),
        ("center right", "center right"),
        ("lower center", "lower center"),
        ("upper center", "upper center"),
        ("center", "center"),
    )

    name = models.fields.CharField(max_length=256)
    description = models.fields.TextField()
    xrange_low = models.fields.FloatField(
        default=1e-11, validators=[validate_finite_positive]
    )
    xrange_high = models.fields.FloatField(
        default=1e-1, validators=[validate_finite_positive]
    )
    yrange_low = models.fields.FloatField(
        default=1e-16, validators=[validate_finite_positive]
    )
    yrange_high = models.fields.FloatField(
        default=1e-6, validators=[validate_finite_positive]
    )
    xlabel = models.fields.CharField(default="mass / eV", max_length=128)
    ylabel = models.fields.CharField(default=r"$g_{a\gamma\gamma} / GeV^{-1}$",
                                     max_length=128)
    show_legend = models.BooleanField(default=True)
    legend_loc = models.CharField(max_length=16,
                                  choices=LEGEND_LOC,
                                  default="best")
    records = models.ManyToManyField(Record, through="Visualization")
    creator = models.ForeignKey(User,
                                null=True,
                                editable=False,
                                on_delete=models.CASCADE)
    creation_date = models.DateField(auto_now_add=True)
    last_update = models.DateField(auto_now=True)

    class Meta:
        permissions = (
            ("view_limitplot", "Can view limitplot"),
        )
        verbose_name = "LimitPlot"

    def __str__(self):
        return self.name

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)
        errors = []
        if self.xrange_low >= self.xrange_high:
            errors.append(ValidationError("xrange is invalid", code="invalid"))
        if self.yrange_low >= self.yrange_high:
            errors.append(ValidationError("yrange is invalid", code="invalid"))
        if errors:
            raise ValidationError(errors)

    def get_absolute_url(self):
        return reverse("limitplot_detail", args=[self.pk])

    def draw(self, visualizations=[]):
        if not visualizations:
            visualizations = list(self.visualization_set.all())

        fig, ax = plt.subplots()
        ax.set_xlim(self.xrange_low, self.xrange_high)
        ax.set_ylim(self.yrange_low, self.yrange_high)
        ax.set_xlabel(self.xlabel)
        ax.set_ylabel(self.ylabel)
        ax.set_xscale("log")
        ax.set_yscale("log")

        for visual in visualizations[::-1]:
            visual.record.draw_on(
                axes=ax,
                label=visual.label,
                color=visual.color,
                linestyle=visual.style,
                alpha=visual.alpha/100.,
                fill=visual.fill,
            )
        if visualizations != [] and self.show_legend:
            ax.legend(loc=self.legend_loc)

        return fig


class Visualization(models.Model):

    LINESTYLES = (
        ("solid", "solid"),
        ("dashed", "dashed"),
        ("dotted", "dotted"),
    )

    record = models.ForeignKey(Record, on_delete=models.CASCADE)
    limitplot = models.ForeignKey(LimitPlot, on_delete=models.CASCADE)
    label = models.fields.CharField(default="", max_length=64, blank=True)
    color = models.fields.CharField(
        default="#1f6cc2",
        max_length=7,
        validators=[
            RegexValidator(re.compile("^#?((?:[0-F]{3}){1,2})$",
                                      re.IGNORECASE)),
        ]
    )
    style = models.CharField(max_length=32, choices=LINESTYLES, default="solid")
    alpha = models.IntegerField(
        default=100,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100),
        ],
    )
    fill = models.BooleanField(default=True)
    draw_order = models.IntegerField(default=0)

    class Meta:
        ordering = ["draw_order"]


@receiver(models.signals.post_save, sender=LimitPlot)
def record_post_save(sender, **kwargs):
    limitplot, created = kwargs["instance"], kwargs["created"]
    if created and limitplot.creator:
        assign_perm("limitplots.view_limitplot", limitplot.creator, limitplot)
        assign_perm("limitplots.change_limitplot", limitplot.creator, limitplot)
        assign_perm("limitplots.delete_limitplot", limitplot.creator, limitplot)
