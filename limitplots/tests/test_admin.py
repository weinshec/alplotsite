from django.contrib.admin.sites import AdminSite
from limitplots.admin import LimitPlotAdmin
from limitplots.models import LimitPlot
from limitplots.tests import create_test_limitplot
from users.tests import UsersTestCase


class MockRequest:
    pass


request = MockRequest()


class LimitPlotAdminTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.limitplot = LimitPlot.objects.create(
            **create_test_limitplot(creator=self.alice, name="Awesome plot"))
        self.site = AdminSite()

    def test_has_image_field(self):
        admin = LimitPlotAdmin(LimitPlot, self.site)
        self.assertIn(
            "image", admin.get_readonly_fields(request, self.limitplot))

    def test_image_method_returns_img_tag(self):
        admin = LimitPlotAdmin(LimitPlot, self.site)
        image = admin.image(self.limitplot)
        self.assertIn("<img", image)
        self.assertIn(self.limitplot.render.url, image)
