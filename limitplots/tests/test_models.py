from django.core.exceptions import ValidationError
from django.test import TestCase
from limitplots.models import Visualization, LimitPlot
from limitplots.tests import create_test_limitplot, create_test_visualization
from records.models import Record
from records.tests import create_test_record


class LimitPlotModel(TestCase):

    def test_can_save_a_webplot_and_retrieve_it_later(self):
        test_limitplot_fields = create_test_limitplot()
        limitplots = LimitPlot.objects.all()

        self.assertEqual(limitplots.count(), 0)
        LimitPlot.objects.create(**test_limitplot_fields)
        self.assertEqual(limitplots.count(), 1)

        for field, content in test_limitplot_fields.items():
            self.assertEqual(getattr(limitplots[0], field), content)

    def test_invalidates_unreasonable_range_fields(self):
        with self.subTest("xrange_high not higher than xrange_low"):
            data = create_test_limitplot(xrange_low=2, xrange_high=1)
            with self.assertRaises(ValidationError):
                LimitPlot(**data).full_clean()

        with self.subTest("yrange_high not higher than yrange_low"):
            data = create_test_limitplot(yrange_low=2, yrange_high=1)
            with self.assertRaises(ValidationError):
                LimitPlot(**data).full_clean()

        with self.subTest("x range must not be zero"):
            data = create_test_limitplot(xrange_low=0, xrange_high=1)
            with self.assertRaises(ValidationError):
                LimitPlot(**data).full_clean()

        with self.subTest("x range must not be negative"):
            data = create_test_limitplot(xrange_low=-1, xrange_high=1)
            with self.assertRaises(ValidationError):
                LimitPlot(**data).full_clean()

        with self.subTest("y range must not be zero"):
            data = create_test_limitplot(yrange_low=0, yrange_high=1)
            with self.assertRaises(ValidationError):
                LimitPlot(**data).full_clean()

        with self.subTest("y range must not be negative"):
            data = create_test_limitplot(yrange_low=-1, yrange_high=1)
            with self.assertRaises(ValidationError):
                LimitPlot(**data).full_clean()

    def test_str_representation_return_name(self):
        limitplot = LimitPlot(**create_test_limitplot(name="foobar"))
        self.assertEqual(str(limitplot), limitplot.name)


class VisualizationModel(TestCase):

    def setUp(self):
        self.record = Record.objects.create(**create_test_record())
        self.limitplot = LimitPlot.objects.create(**create_test_limitplot())

    def test_can_add_visualizations_and_retrieve_them_later(self):
        visualizations = Visualization.objects.all()

        self.assertEqual(visualizations.count(), 0)

        test_visualization_fields = create_test_visualization()
        Visualization.objects.create(record=self.record,
                                     limitplot=self.limitplot,
                                     **test_visualization_fields)
        self.assertEqual(visualizations.count(), 1)

        visualization = visualizations[0]
        for field, content in test_visualization_fields.items():
            self.assertEqual(getattr(visualization, field), content)

        self.assertEqual(visualization.record, self.record)
        self.assertEqual(visualization.limitplot, self.limitplot)
        self.assertIn(self.record, self.limitplot.records.all())
        self.assertIn(self.limitplot, self.record.limitplot_set.all())
