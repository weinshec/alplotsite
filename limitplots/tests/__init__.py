def create_test_limitplot(**kwargs):
    test_limitplot = dict(
        name="Awesome Plot",
        description="Some description",
        xrange_low=1,
        xrange_high=5,
        yrange_low=1,
        yrange_high=5,
        xlabel="x-axis",
        ylabel="y-axis",
        show_legend=True,
        legend_loc="best",
    )
    test_limitplot.update(kwargs)

    return test_limitplot


def create_test_visualization(**kwargs):
    test_visualization = dict(
        label="data label",
        color="#1f32c1",
        style="dashed",
        alpha=90,
        fill=True,
        draw_order=0,
    )
    test_visualization.update(kwargs)

    return test_visualization


def create_formset_data(models=[], total=None, initial=0, max_num=1000,
                        prefix="formset", **kwargs):

    if not total:
        total = len(models)

    formset = {
        "{}-TOTAL_FORMS".format(prefix): "{}".format(total),
        "{}-INITIAL_FORMS".format(prefix): "{}".format(initial),
        "{}-MAX_NUM_FORMS".format(prefix): "{}".format(max_num),
    }

    for i, model in enumerate(models):
        for field, content in model.items():
            formset["{}-{}-{}".format(prefix, i, field)] = "{}".format(content)

    formset.update(kwargs)

    return formset


def formset_to_dict(formset):
    data = dict()
    for k, v in formset.management_form.initial.items():
        data["{}-{}".format(formset.prefix, k)] = v
    for i, form in enumerate(formset):
        for k, v in form.initial.items():
            data["{}-{}-{}".format(formset.prefix, i, k)] = v
    return data
