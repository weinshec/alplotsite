from limitplots.models import LimitPlot, Visualization
from limitplots.forms import visualization_formset
from limitplots.tests import create_test_limitplot, create_test_visualization,\
                             create_formset_data
from records.models import Record
from records.tests import create_test_record
from users.tests import UsersTestCase


class VisualizationInlineFormSetTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.limitplot = LimitPlot.objects.create(
            **create_test_limitplot(creator=self.alice))
        self.record = Record.objects.create(
            **create_test_record(creator=self.alice))

        self.visualization = Visualization(limitplot=self.limitplot,
                                           record=self.record,
                                           **create_test_visualization())
        self.visualization.save()

    def test_empty_formset_has_extra_row(self):
        formset = visualization_formset()
        self.assertEqual(formset.total_form_count(), 1)
        self.assertEqual(formset.initial_form_count(), 0)
        self.assertEqual(len(formset.extra_forms), 1)

    def test_formset_considers_empty_render_forms_as_to_delete(self):
        formset_data = create_formset_data(
            total=2, initial=0,
            models=[
                create_test_visualization(),
                create_test_visualization(record=self.record.pk)
            ]
        )
        formset = visualization_formset(
            formset_data, prefix="formset", form_kwargs={"user": self.alice})
        # Need to pass user here, as records would not empty otherwise
        self.assertEqual(formset.total_form_count(), 2)
        self.assertEqual(len(formset.deleted_forms), 1)
