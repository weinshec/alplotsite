(function($) {
  $.fn.ajaxRecordQuery = function() {
    $(this).change(function() {
      var url = $("#id_limitplot_form").attr("data-records-url");
      var query = $(this).val();
      var target = $(this).siblings(".record-list");
      $.ajax({
        url: url,
        data: {
          'query': query
        },
        success: function (data) {
          target.html(data);
          $(".record-li").click(function() {
            var reveal_id = $(this).parents(".reveal").attr("id");
            var reveal_button = $("#"+reveal_id+"_button");
            var record_pk = $(this).attr("data-record-pk");
            var record_title = $(this).attr("data-record-title");

            reveal_button.siblings(".record-select").val(record_pk);
            reveal_button.siblings(".record-title").html(record_title);
          });
        }
      });
    });
    return this;
  }
})( jQuery );


$(document).ready(function(){

  // Add formset.js to #visual-formset and attach to the relflow event firing
  // when a new form has been added in order to reload foundation on the new
  // form (=event.target)
  $("#visual-formset").formset({
    animateForms: true,
    reorderMode: 'dom',
  }).on("reflow", function(event) {
    $(event.target).foundation();
    $(".record-query").ajaxRecordQuery();
  });

  // Add webplots preview function
  $("#id_limitplot_form").webplots({
    show_errors: true
  });

});

